# Installation
> `npm install --save @types/prosemirror-schema-list`

# Summary
This package contains type definitions for prosemirror-schema-list (https://github.com/ProseMirror/prosemirror-schema-list).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/prosemirror-schema-list.

### Additional Details
 * Last updated: Wed, 17 Feb 2021 16:27:06 GMT
 * Dependencies: [@types/orderedmap](https://npmjs.com/package/@types/orderedmap), [@types/prosemirror-model](https://npmjs.com/package/@types/prosemirror-model), [@types/prosemirror-state](https://npmjs.com/package/@types/prosemirror-state)
 * Global values: none

# Credits
These definitions were written by [Bradley Ayers](https://github.com/bradleyayers), [David Hahn](https://github.com/davidka), [Tim Baumann](https://github.com/timjb), and [Patrick Simmelbauer](https://github.com/patsimm).
